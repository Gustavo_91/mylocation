# My Location

This application keeps track of the user's current location to show the routes traveled

## Getting Started

* Clone the project to your desired path

### Prerequisites

* Xcode version equal or greater than 9.3
* This app works on portrait mode, so turning the device has no effect at all
* Iteraction with app's maps are disabled (map's purpose is just follow the user)

### Installing

* Run the app in Xcode
* If you run the app in the simulator, use some pre-configured location route, For Example: 
**```Debug -> Location -> Freeway drive```**
* When you are asked for the location permit, accept it.

### Tips for using it
* When the tracking switch is off (red) the app follow the user but it doesn't do anything more
* When the switch is on the app tracks the user movement, it leaves a blue path as the user move
* When you turn on and then turn of the switch, the app create a journey that you can see in a list
* When you select a journet you will see the start and times and a map that show the journey's track in a blue path  

**Remember Turn on the swith to track the user movement and then turn off to create a journey**  
**Note: The blue path that is drawn when the user moves only appears when the switch is on**

## Authors

* **Gustavo Pirela**

