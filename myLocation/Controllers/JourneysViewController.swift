//
//  JourneysViewController.swift
//  myLocation
//
//  Created by Gustavo Pirela on 22/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class JourneysViewController: UIViewController {
    
    // MARK: - Properties
    
    var journeys: [Journey] {
        return Journey.journeys
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var journeysTable: UITableView!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureJourneysTable()
    }
    
    // MARK: - Private
    
    private func configureJourneysTable() {
        journeysTable.delegate = self
        journeysTable.dataSource = self
        journeysTable.tableFooterView = UIView()
    }
    
    private func navigateToJourneyDetailViewController(journeyIndex: Int) {
        let viewController = JourneyDetailViewController(nibName: "JourneyDetailViewController",
                                                         bundle: nil)
        viewController.journeyIndex = journeyIndex
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension JourneysViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return journeys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.standard) else {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.CellIdentifier.standard)
            let cell = UITableViewCell(style: .default, reuseIdentifier: Constants.CellIdentifier.standard)
            cell.textLabel?.text = journeys[indexPath.row].name
            return cell
        }
        
        cell.textLabel?.text = journeys[indexPath.row].name
        return cell
    }
    
}

extension JourneysViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToJourneyDetailViewController(journeyIndex: indexPath.row)
    }
    
}


