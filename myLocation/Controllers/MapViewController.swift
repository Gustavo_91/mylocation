//
//  MapViewController.swift
//  myLocation
//
//  Created by Gustavo Pirela on 22/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    // MARK: - Properties
    
    let locationManager = LocationManager.shared
    let regionSpan = MKCoordinateSpan(latitudeDelta: Constants.Region.size,
                                      longitudeDelta: Constants.Region.size)
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var trackingSwitch: UISwitch!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationController()
        configureSwitch()
        configureMapView()
    }
    
    func drawBackgroundLocationsOnMap() {
        let lineCoordinates = UserLocation.backgroundLocations.map {return $0.coordinate}
        let polyLine = MKPolyline(coordinates: lineCoordinates, count: lineCoordinates.count)
        mapView.add(polyLine)
        UserLocation.backgroundLocations.removeAll()
    }
    
    // MARK: - Private
    
    private func configureMapView() {
        mapView.isUserInteractionEnabled = false
        mapView.isZoomEnabled = false
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.region.span = regionSpan
        mapView.mapType = .standard
        mapView.setUserTrackingMode(.follow, animated: true)
    }
    
    private func configureSwitch() {
        trackingSwitch.isOn = false
        trackingSwitch.tintColor = UIColor.red
        trackingSwitch.layer.cornerRadius = trackingSwitch.frame.height / 2
        trackingSwitch.backgroundColor = UIColor.red
        trackingSwitch.addTarget(self, action: #selector(trackJourney), for: .valueChanged)
    }
    
    private func configureNavigationController() {
        let journeysButtonItem = UIBarButtonItem(title: "Journeys",
                                             style: .plain,
                                             target: self,
                                             action: #selector(navigateToJourneysViewController))
        
        navigationItem.rightBarButtonItem = journeysButtonItem
    }
    
    // MARK: - Selectors
    
    @objc func trackJourney() {
        if trackingSwitch.isOn {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.stopUpdatingLocation()
            _ = Journey(locations: UserLocation.locations)
            UserLocation.locations.removeAll()
        }
    }
    
    @objc func navigateToJourneysViewController() {
        let viewController = JourneysViewController(nibName: Constants.NibName.journeys,
                                                    bundle: nil)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension MapViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        guard let lastRecordedLocation = UserLocation.locations.last else {
            return
        }
        
        let lineCoordinates = [lastRecordedLocation.coordinate, userLocation.coordinate]
        let polyLine = MKPolyline(coordinates: lineCoordinates, count: lineCoordinates.count)
        mapView.add(polyLine)
    }
    
}


