//
//  JourneyDetailViewController.swift
//  myLocation
//
//  Created by Gustavo Pirela on 23/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import MapKit

class JourneyDetailViewController: UIViewController {
    
    // MARK: - Properties
    
    var journey: Journey? {
        guard journeyIndex < Journey.journeys.count else {
            return nil
        }
        
        return Journey.journeys[journeyIndex]
    }
    
    var journeyIndex: Int = 0
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMapView()
        configureLabels()
    }
    
    // MARK: - Private
    
    private func configureLabels() {
        guard let journey = journey else {
            return
        }
        
        startDateLabel.text = journey.startDate
        endDateLabel.text = journey.endDate
    }
    
    private func configureMapView() {
        configureMapViewRegion()
        drawJourneyTrackOnMap()
    }
    
    private func configureMapViewRegion() {
        guard let journey = journey else {
            return
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        let mapSpan = MKCoordinateSpan(latitudeDelta: Constants.Region.size,
                                       longitudeDelta: Constants.Region.size)
        
        let region = MKCoordinateRegion(center: journey.locations[0].coordinate, span: mapSpan)
        mapView.setRegion(region, animated: true)
    }
    
    private func drawJourneyTrackOnMap() {
        guard let journey = journey else {
            return
        }
        
        let coordinates = journey.locations.map {return $0.coordinate}
        let polyLine = MKPolyline(coordinates: coordinates, count: coordinates.count)
        mapView.add(polyLine)
    }
    
}

extension JourneyDetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
}
