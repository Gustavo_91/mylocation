//
//  AppDelegate.swift
//  myLocation
//
//  Created by Gustavo Pirela on 22/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let mainViewController = MapViewController(nibName: Constants.NibName.map,
                                               bundle: nil)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureWindow()
        configureLocationManager()
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        mainViewController.drawBackgroundLocationsOnMap()
    }

    // MARK: - Private
    
    private func configureWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = configureNavigationController()
        window?.makeKeyAndVisible()
    }
    
    private func configureNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.viewControllers = [mainViewController]
        return navigationController
    }
    
    private func configureLocationManager() {
        let locationManager = LocationManager.shared
        locationManager.delegate = LocationDelegate.shared
    }

}

