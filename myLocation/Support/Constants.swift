//
//  Constants.swift
//  myLocation
//
//  Created by Gustavo Pirela on 23/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

// Organize constants used by the app in segmented sub structs
struct Constants {
    
    struct Region {
        static let size = 0.005
    }
    
    struct NibName {
        static let map = "MapViewController"
        static let journeys = "JourneysViewController"
        static let journeyDetail = "JourneyDetailViewController"
    }
    
    struct CellIdentifier {
        static let standard = "standard"
    }
    
    struct DateFormat {
        static let short = "HH:mm:ss"
    }
    
}
