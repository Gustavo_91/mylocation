//
//  LocationManager.swift
//  myLocation
//
//  Created by Gustavo Pirela on 22/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class LocationManager {
    
    static let shared: CLLocationManager = {
        let manager = CLLocationManager()
        configure(locationManager: manager)
        return manager
    }()
    
    private init() {}
    
    private static func configure(locationManager: CLLocationManager) {
        guard CLLocationManager.locationServicesEnabled() == true else {
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.pausesLocationUpdatesAutomatically = false
    }
}
