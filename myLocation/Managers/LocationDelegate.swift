//
//  LocationDelegate.swift
//  myLocation
//
//  Created by Gustavo Pirela on 23/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class LocationDelegate: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationDelegate()
    
    private override init() {}
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let userLocation = locations.last else {
            return
        }
        
        _ = UserLocation(timeStamp: userLocation.timestamp, coordinate: userLocation.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        guard let error = error as? CLError.Code else {
            return
        }
        
        if error == CLError.denied {
            manager.stopUpdatingLocation()
        }
    }

}
