//
//  Date+Format.swift
//  myLocation
//
//  Created by Gustavo Pirela on 23/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

extension Date {
    
    func getFormattedDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateFormat.short
        return dateFormatter.string(from: self)
    }
    
}
