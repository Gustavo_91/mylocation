//
//  UserLocation.swift
//  myLocation
//
//  Created by Gustavo Pirela on 22/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

struct UserLocation {
    
    static var locations = [UserLocation]()
    static var backgroundLocations = [UserLocation]()
    
    // MARK: - Properties
    
    var timeStamp: Date
    var coordinate: CLLocationCoordinate2D
    
    // MARK: - Initialization
    
    init(timeStamp: Date, coordinate: CLLocationCoordinate2D) {
        self.timeStamp = timeStamp
        self.coordinate = coordinate
        UserLocation.locations.append(self)
        
        if UIApplication.shared.applicationState == .background {
            UserLocation.backgroundLocations.append(self)
        }
    }
    
}
