//
//  Journey.swift
//  myLocation
//
//  Created by Gustavo Pirela on 22/10/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

struct Journey {
    
    static var journeys = [Journey]()
    
    // MARK: - Properties
    
    let name = "Journey \(Journey.journeys.count + 1)"
    let locations: [UserLocation]
    var startDate: String? { return getStartDate() }
    var endDate: String? { return getEndDate() }
    
    // MARK: - Initialization
    
    init(locations: [UserLocation]) {
        self.locations = locations
        Journey.journeys.append(self)
    }
    
    // MARK: - Private
    
    private func getStartDate() -> String? {
        guard let startLocation = locations.first else {
            return nil
        }
        
        return startLocation.timeStamp.getFormattedDate()
    }
    
    private func getEndDate() -> String? {
        guard let endLocation = locations.last else {
            return nil
        }
        
        return endLocation.timeStamp.getFormattedDate()
    }
    
}
